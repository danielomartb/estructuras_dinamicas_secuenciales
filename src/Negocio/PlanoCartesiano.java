/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Punto;
import java.util.Random;
import ufps.util.colecciones_seed.VectorGenerico;

/**
 * Clase para manejar puntos y los crea de manera aleatoria
 * @author madarme
 */
public class PlanoCartesiano {
    
    private VectorGenerico<Punto> myPtos;

    public PlanoCartesiano() {
    }
    
    public PlanoCartesiano(int cantidad) {
        
        if(cantidad <=0)
             throw new RuntimeException("Cantidad no válida");
        
        //Crear puntos:
        this.myPtos=new VectorGenerico(cantidad);
        //llamar crearPuntos
        this.crearPuntos_Aleatorios();
    }
    
    private void crearPuntos_Aleatorios()
    {
        for(int i=0;i<this.myPtos.length();i++)
        {
            Random numero=new Random();
            float x=numero.nextFloat();
            float y=numero.nextFloat();
            Punto nuevo=new Punto(x,y);
            this.myPtos.add(nuevo);
        }
    }

    @Override
    public String toString() {
        String msg="";
        for(int i=0;i<this.myPtos.length();i++)
            msg+=this.myPtos.get(i).toString()+"\t";
        
        return msg;
    }
    
    
}
