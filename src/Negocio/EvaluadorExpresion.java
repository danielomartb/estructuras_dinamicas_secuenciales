/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Invitado
 */
public class EvaluadorExpresion {

    private String expresion;

    public EvaluadorExpresion() {
    }

    public EvaluadorExpresion(String expresion) {
        this.expresion = expresion;
    }

    public boolean evaluarParentesis() {
        if (this.expresion.isEmpty()) {
            throw new RuntimeException("No se puede realiazar el proceso por que la cadena es vacía");
        }

        //La pila de evaluación:
        Pila<String> p = new Pila();
        for (int i = 0; i < this.expresion.length(); i++) {

            if (this.expresion.charAt(i) == '(') {
                p.apilar("(");
            } else {
                if (this.expresion.charAt(i) == ')') {
                    if (p.esVacio()) {
                        return false;
                    }
                    p.desapilar();
                }
            }
        }
        return p.esVacio();
    }

    @Override
    public String toString() {
        return "EvaluadorExpresion{" + "expresion=" + expresion + '}';
    }

}
