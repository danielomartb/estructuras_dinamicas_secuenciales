/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.colecciones_seed.IteratorLCD;
import ufps.util.colecciones_seed.ListaCD;

/**
 * SOLO VA USAR ITERADORES PARA RESOLVER LOS MÉTODOS
 * @author madar
 */
public class SistemaNumerico {
    
    private ListaCD<Integer> myLista1;
    private ListaCD<Integer> myLista2;

    public SistemaNumerico() {
    }
    
    public SistemaNumerico(int tam_lista1, int tam_lista2) {
        
        this.myLista1=this.crearLista(tam_lista1);
        this.myLista2=this.crearLista(tam_lista2);
         
    }

    
    
    public ListaCD<Integer> getUnion()
    {
        if(myLista1.esVacia() || myLista2.esVacia()){
        throw new RuntimeException("No se puede realizar el proceso");
        }
        ListaCD<Integer>nueva=new ListaCD();
        nueva=myLista1.clonar();
        IteratorLCD it = (IteratorLCD) this.myLista2.iterator();
        
        while(it.hasNext()){
        Integer dato = (Integer) it.next();
        nueva.insertarFin(dato);
        }
        return nueva;
    }
    
    
    
    public ListaCD<Integer> getInterseccion() {
        if(myLista1.esVacia() || myLista2.esVacia()){
        throw new RuntimeException("No se puede realizar el proceso");
        }
        ListaCD<Integer> listaNueva = new ListaCD();
        IteratorLCD it1 = (IteratorLCD) this.myLista1.iterator();
        IteratorLCD it2 = (IteratorLCD) this.myLista2.iterator();
        Integer dato1, dato2, coincidencias = 0;
        while (it1.hasNext()) {
            dato1 = (Integer) it1.next();
            coincidencias = 0;
            while (it2.hasNext()) {
                dato2 = (Integer) it2.next();
                if (dato1.equals(dato2)) {
                    coincidencias++;
                    break;
                }
            }
            if (coincidencias > 0) {
                listaNueva.insertarFin(dato1);
            }
        }
        return listaNueva;
    }

    
    public ListaCD<Integer> getDiferencia(ListaCD<Integer> myLista1) {
        ListaCD<Integer> nueva = new ListaCD();
        ListaCD<Integer> interseccion = this.getInterseccion();

        IteratorLCD it1 = (IteratorLCD) myLista1.iterator();
        while (it1.hasNext()) {
            Integer dato1 = (Integer) it1.next();
            IteratorLCD it2 = (IteratorLCD) interseccion.iterator();
            int cont = 0;
            while (it2.hasNext()) {
                Integer dato2 = (Integer) it2.next();
                if (dato1.equals(dato2)) {
                    cont++;
                    break;
                }

            }
            if (cont == 0) {
                nueva.insertarFin(dato1);
            }

        }
        return nueva;
    }
    
    public ListaCD<Integer> getDiferenciaAsimetrica() {
        ListaCD<Integer> nueva = getDiferencia(myLista1);
        ListaCD<Integer> l2 = getDiferencia(myLista2);
        IteratorLCD<Integer> it2 = (IteratorLCD<Integer>) l2.iterator();
        Integer dato;
        while (it2.hasNext()) {
            dato = it2.next();
            nueva.insertarFin(dato);
        }
        return nueva;
    }

    
    
    /**
     * 
     * @param n inicio
     * @param m fin
     * @return 
     */
    private ListaCD<Integer> crearListaRandom(int tamanio, int n) {
        ListaCD<Integer> l = new ListaCD();
        while (tamanio-- > 0) {
            //a partir de: http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
            int valorDado = (int)((Math.random())*n)+1;//Desde m hasta n
            l.insertarInicio(valorDado);
        }
        return l;
    }
    
    private ListaCD<Integer> crearLista(int n) {
        ListaCD<Integer> l = new ListaCD();
        while (n-- > 0) {
            //a partir de: http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
             l.insertarInicio(n);
        }
        return l;
    }

    public Object getMyLista1() {

        return myLista1;
        }

    public Object getMyLista2() {
        return myLista2;
    }
    
    
}
