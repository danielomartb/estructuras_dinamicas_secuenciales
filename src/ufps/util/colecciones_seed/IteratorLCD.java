/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;

/**
 * Clase para el manejo interno de un iterador en listas circulares dobles
 * @author madarme
 */
public class IteratorLCD<T> implements Iterator<T> {

    private NodoD<T> cabezaIterador;
    private NodoD<T> posicion_actual;

    public IteratorLCD(NodoD<T> cabezaIterador) {
        this.cabezaIterador = cabezaIterador;
        this.posicion_actual = this.cabezaIterador.getSig();
    }

    @Override
    public boolean hasNext() {
        return this.posicion_actual != this.cabezaIterador;
    }

    @Override
    public T next() {
        if (this.hasNext()) {
            this.posicion_actual = this.posicion_actual.getSig();
            return this.posicion_actual.getAnt().getInfo();
        }
        return null;
    }

    
}
