/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * Implementación de una pila usando ListaCD
 *
 * @author Madarme
 */
public class Pila<T> {

    private ListaCD<T> myLista = new ListaCD();

    public Pila() {
    }

    public void apilar(T info) {
        this.myLista.insertarInicio(info);
    }

    public T desapilar() {
        if (this.esVacio()) {
            throw new RuntimeException("Pila vacía");
        }
        return this.myLista.eliminar(0);
    }

    public boolean esVacio() {
        return this.myLista.esVacia();
    }

    public int size() {
        return this.myLista.getTamanio();
    }

    public String toString() { //

        //String msj = myLista.toString();
        return "Tope-->"+myLista.toStringPilasColas()+ "null";
    }
    public Pila clonarPila(){
        Pila<T> pilaClonada = new Pila<>();
        ListaCD<T> l= new ListaCD<>();
        l=myLista.clonarEInvertir();
        while(!l.esVacia()){
        pilaClonada.apilar(l.eliminar(0));
        
        }
        
    return pilaClonada;
    }
}
