/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que modela una lista circular doblemente enlazada con nodo
 * centinela(nodo cabecera)
 *
 * @author Daniel Omar Tijaro Beltran Codigo: 1151859 Estructuras de datos - C
 * 04/05/2021--> 10:45
 */
public class ListaCD<T> implements Iterable<T>{

    private NodoD<T> cabeza;
    private int tamanio = 0;

    public ListaCD() {
        //Crear el nodo centinela
        this.crearNodoCentinela();
    }

    private void crearNodoCentinela() {
        //Crear el nodo centinela
        this.cabeza = new NodoD();
        //Esto sobra , solo se realiza por definición formal
        this.cabeza.setInfo(null);
        //Crear los enlaces dobles o anillo:
        this.cabeza.setSig(this.cabeza);
        this.cabeza.setAnt(this.cabeza);

    }

    
    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public void set(int i, T datoNuevo) {

        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }
    
    
    
    
    
    
    public int getTamanio() {
        return tamanio;
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        this.cabeza.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.tamanio++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        nuevo.getAnt().setSig(nuevo);
        this.cabeza.setAnt(nuevo);
        this.tamanio++;
    }

    public String toStringPilasColas() {

        if (this.esVacia()) {
            return "La lista no contiene elementos";
        }

        String msg = "";
        //vector--> pos en 0 --> hasta length-1
        //Desde el siguiente de la cabeza hasta que llegue a la cabeza(circular)
        NodoD<T> nodoActual = this.cabeza.getSig();
        while (nodoActual != this.cabeza) {
            T info = nodoActual.getInfo();
            //Para esto es necesario que la clase en T tenga toString
            msg += info.toString() + "<->";
            nodoActual = nodoActual.getSig();
        }
       return   msg  ;
    }
    
        @Override
    public String toString() {

        if (this.esVacia()) {
            return "La lista no contiene elementos";
        }

        String msg = "";
        //vector--> pos en 0 --> hasta length-1
        //Desde el siguiente de la cabeza hasta que llegue a la cabeza(circular)
        NodoD<T> nodoActual = this.cabeza.getSig();
        while (nodoActual != this.cabeza) {
            T info = nodoActual.getInfo();
            //Para esto es necesario que la clase en T tenga toString
            msg += info.toString() + "<->";
            nodoActual = nodoActual.getSig();
        }
       return "Cabeza<-->" + msg+"<-->Cabeza"  ;
    }

    public boolean esVacia() {
        // return this.tamanio==0
        return this.cabeza == this.cabeza.getSig() && this.cabeza == this.cabeza.getAnt();
    }

    public ListaCD clonar() {
        ListaCD<T> clonada = new ListaCD<T>();
        NodoD<T> info = this.cabeza.getSig();
        for (int i = 0; i < this.tamanio; i++) {
            clonada.insertarFin(info.getInfo());
            info = info.getSig();
        }
        return clonada;
    }
    
    
        public ListaCD clonarEInvertir() {
        ListaCD<T> clonada = new ListaCD<T>();
        NodoD<T> info = this.cabeza.getSig();
        for (int i = 0; i < this.tamanio; i++) {
            clonada.insertarInicio(info.getInfo());
            info = info.getSig();
        }
        return clonada;
    }

    public T eliminar(int i) {

        try {
            NodoD<T> nodoBorrar = this.getPos(i);
            nodoBorrar.getAnt().setSig(nodoBorrar.getSig());
            nodoBorrar.getSig().setAnt(nodoBorrar.getAnt());
            nodoBorrar.setSig(null);
            nodoBorrar.setAnt(null);
            this.tamanio--;
            return nodoBorrar.getInfo();

        } catch (Exception ex) {

            System.err.println("Error:" + ex.getMessage());
        }
        return null;
    }

    private NodoD<T> getPos(int i) throws Exception //Excepción obligatoria su tratamiento
    {
        if (i < 0 || i >= this.tamanio) {
            throw new Exception("índice fuera de rango:" + i);
        }

        NodoD<T> nodoPos = this.cabeza.getSig();
        while (i > 0) {
            nodoPos = nodoPos.getSig();
            i--;
        }
        return nodoPos;
    }

    public void borrarLista() {
        this.crearNodoCentinela();
        this.tamanio = 0;
    }

    public void enlazarLista(NodoD<T> nodoInicio, ListaCD<T> listaEnlazar, NodoD<T> nodoFin, int tamPatron) {

        nodoInicio.getAnt().setSig(listaEnlazar.cabeza.getSig());//l1-->l3
        listaEnlazar.cabeza.getAnt().setSig(nodoFin.getSig());   //L1-->L3-->L1
        listaEnlazar.cabeza.getSig().setAnt(nodoInicio.getAnt());//L1<-->l3-->L1
        nodoFin.getSig().setAnt(listaEnlazar.cabeza.getAnt());   //L1<-->L3<-->L1
        this.tamanio += listaEnlazar.tamanio - tamPatron;        //Modifico el tamaño

    }

    /**
     * Ejercicio 14 de la hoja de listas simples
     *
     * @param patron una lista de objetos a buscar dentro de la lista original
     * @param nueva la lista a reemplazar
     * @return la cantidad de veces que realizó el reemplazo
     */
    public int reemplazar(ListaCD<T> patron, ListaCD<T> nueva) throws Exception {
        //this=l1, patron=l2, nueva=l3
        if (this.esVacia() || patron.esVacia() || nueva.esVacia()) {
            throw new Exception("NO SE PUEDE REALIZAR EL PROCESO DE REEMPLAZO PORQUE UNA DE LAS 3 LISTAS ES VACIA");
        }
        ListaCD<T> copia = new ListaCD();
        copia = nueva.clonar();
        NodoD<T> inicioPatron = patron.cabeza.getSig();
        NodoD<T> nodoAct = this.cabeza.getSig();
        NodoD<T> nodoActPatron = patron.cabeza.getSig();
        int contador = 0, remplazos = 0;
        boolean modificacionReciente = false, tieneSiguiente = false, patronEcontrado = false,
                tieneCoincidencias = false;

        while (nodoAct != this.cabeza) {                            //Mientras que no sea cabeza
            if (modificacionReciente) {                             //Vuelvo a 0 el contador despues de una modificacion reciente
                contador = 0;
                modificacionReciente = false;
            }
            if (nodoAct.getInfo().equals(patron.cabeza.getSig().getInfo()) && contador == 0) {
                inicioPatron = nodoAct;                             //Guardo el inicio del patron solo cuando encuentre la primera coincidencia
            }
            if (nodoAct.getInfo().equals(nodoActPatron.getInfo())) {//Itero el contador cuando encuentre iguales
                contador++;
            } else {
                contador = 0;
            }
            patronEcontrado = contador == patron.tamanio;           //Verifico si recorrio todo el patron, osea si halló el patron en L1
            if (patronEcontrado) {
                this.enlazarLista(inicioPatron, copia, nodoAct, patron.tamanio);//Invocacion del metodo
                modificacionReciente = true;                        //Uso esta variable para cuando el siguiente patron esta justo despues de hacer un reemplazo
                copia = nueva.clonar();                             //Creo una nueva copia solo cuando se use la que ya esta creada
                nodoActPatron = patron.cabeza.getSig();             //Al encontrar el patron vuelvo el nodo que lo recorre a su posicio inicial
                remplazos++;

            }
            tieneCoincidencias = contador > 0 && contador < patron.tamanio;
            if (tieneCoincidencias) {                               //Solo itero el patron a buscar cuando halla al menos una coinciencia y aun no se halle el patron 
                tieneSiguiente = nodoActPatron.getSig() != patron.cabeza;
                if (tieneSiguiente) {                               //Verifico que el nodo siguiente del nodo actual del patron no sea cabeza e itero una vez
                    nodoActPatron = nodoActPatron.getSig();         //Pero si el siguiente es cabeza lo itero dos veces para no quedar en el info de la cabeza(null)
                } else {
                    nodoActPatron = nodoActPatron.getSig().getSig();
                }
            }
            nodoAct = nodoAct.getSig();                              //Itero el nodo actual de la lista en cada ciclo
        }
        System.out.println("El tamaño de L1 quedo: " + this.tamanio);//Imprimo el tamaño por verificar que haga la operacion correcta
        return remplazos;                                              //Retorno la cantidad de reemplazos
    }
    
        @Override
    public Iterator<T> iterator() {
        return new IteratorLCD<>(this.cabeza);
    }
}
