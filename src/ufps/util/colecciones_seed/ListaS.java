 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa la colección Lista Simple enlazada
 * @author madarme
 */
public class ListaS<T> {
    
    //No hace falta la inicialización (opcional)
    private Nodo<T> cabeza=null;
    private int tamanio=0; //<--Sólo se altera con las operaciones de inserción y/o borrado

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }
    /**
     * Método adiciona un elemento al inicio de la lista
     * @param datoNuevo objeto a insertar en la lista simple
     */
    
    
    
    public void insertarInicio(T datoNuevo)
    {
    /**
     * Estrategia computacional:
            1. Objeto nuevo en Nodo_nuevo
            2. El sig de Nuevo_nodo es la cabeza
            3. cabeza=Nuevo_nodo
            4. aumentar tamaño
     */
        
        Nodo<T> nuevoNodo=new Nodo(datoNuevo,this.cabeza);
        this.cabeza=nuevoNodo;
        this.tamanio++;
    }

    public void insertarFin(T datoNuevo)
    {
        if(this.esVacia())
            this.insertarInicio(datoNuevo);
        else
        {
        
            
            try {
                Nodo<T> nodoNuevo=new Nodo(datoNuevo, null);
                Nodo<T> nodoUltimo=getPos(this.tamanio-1);
                //Unimos:
                nodoUltimo.setSig(nodoNuevo);
                this.tamanio++;
                
                
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
            
        }
    }
    
    
    public T get(int i)
    {
        
        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
    
    
    public void set(int i,T datoNuevo)
    {
        
        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            
        }
    }
    
    
    /**
     * Método borrar el nodo de la posición i
     * @param i un entero que representa el índice de un nodo
     * @return el objeto almacenado en esa posición
     */
    public T eliminar(int i)
    {
        if(this.esVacia())
            return null;
        //Caso 1: i en cabeza , i==0
        Nodo<T> nodoBorrar=null;
        if(i==0)
        {
            nodoBorrar=this.cabeza;
            this.cabeza=this.cabeza.getSig();
            
        }
        else
        {
            try {
                Nodo<T> nodoAnterior=this.getPos(i-1);
                nodoBorrar=nodoAnterior.getSig();
                //Unir :
                nodoAnterior.setSig(nodoBorrar.getSig());
                
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }
        
        nodoBorrar.setSig(null);
        this.tamanio--;
        return nodoBorrar.getInfo();
    }
    
    
    private Nodo<T> getPos(int i) throws Exception //Excepción obligatoria su tratamiento
    {
        if(i<0 || i>=this.tamanio)
            throw new Exception("índice fuera de rango:"+i);
        
        Nodo<T> nodoPos=this.cabeza;
        while(i>0)
        {
            nodoPos=nodoPos.getSig();
            i--;
        }
        return nodoPos;
    }
    
    @Override
    public String toString() {
        
        if(this.esVacia())
            return "La lista no contiene elementos";
        
        String msg="";
        //vector--> pos en 0 --> hasta length-1
        //Desde la cabeza hasta null
        Nodo<T> nodoActual=this.cabeza;
        while(nodoActual!=null)
        {
            T info=nodoActual.getInfo();
            //Para esto es necesario que la clase en T tenga toString
            msg+=info.toString()+"->";
            nodoActual=nodoActual.getSig();
        }
     return "Cabeza->"+msg+"null";
    }
    
    public boolean esVacia()
    {
        return this.cabeza==null;
    }
    
    /**
     * Elimina todo dato repetido de la lista simple 
     * y deja una sóla incidencia
     * Por ejemplo: L=<3,5,6,3,3,5,7,8> 
     *  L.eliminarRepetidos()--> L=<3,5,6,7,8>
     * 
     * --> 2 primeros ejercicios reciben un bono de 0.5 sobre el P1
     */
    /*public void eliminarRepetidos() throws Exception
    {
       Nodo<T> nodoAnt=cabeza;
       Nodo<T> nodoSig=null;
       Nodo<T> desEnlazar=nodoAnt;
       
       if(nodoAnt.getSig()!=null){
        nodoSig=nodoAnt.getSig();
       }
       
                                                                     
        while(getPos(j).getSig()!=nullnodoSig!=null){ 5-->4-->5-->4-->3-->2-->1-->2-->3-->null
      
             System.out.println("i:");
             
          while(nodoSig.getSig()!=null){
              if(nodoAnt.getInfo()==nodoSig.getInfo()){
                  
                desEnlazar.setSig(nodoSig.getSig());
                nodoSig.setSig(null);
                tamanio--;  
                  
             }
              nodoSig=nodoSig.getSig();
              desEnlazar=desEnlazar.getSig();
             
           }
        nodoAnt=nodoAnt.getSig();
        desEnlazar=nodoAnt;
        nodoSig=nodoAnt.getSig();
     
        }
    }
   */
    
     public void eliminarRepetido()
    {
        // :)
        T elementosVistos[] = (T[]) new Object[this.tamanio]; 
        boolean repetidos[] = new boolean[this.tamanio];
        
        int i = this.tamanio;
        int elementosAgregados = 0;
        Nodo<T> nodoActual = this.cabeza;
        Nodo<T> nodoAnterior = null;
        
        while (i-- > 0) {
            boolean exist = false;
            for (int j = 0; j < elementosAgregados && !exist; j++) {
                if (elementosVistos[j].equals(nodoActual.getInfo())) {
                    repetidos[j] = true;         
                    exist = true;
                    nodoAnterior.setSig(nodoActual.getSig());
                    nodoActual.setSig(null);
                    this.tamanio--;
                }
            }
            if (!exist) {
                elementosVistos[elementosAgregados] = nodoActual.getInfo();
                elementosAgregados++;
                nodoAnterior = nodoActual;
                nodoActual = nodoActual.getSig();
            }
            else {
                nodoActual = nodoAnterior.getSig();
            }
        }
        
        nodoActual = this.cabeza;
        nodoAnterior = null;
        for (int j = 0; j < elementosAgregados; j++) {
            if (repetidos[j]) {
                if (nodoAnterior == null) {
                    this.cabeza = nodoActual.getSig();
                    nodoActual.setSig(null);
                    nodoActual = this.cabeza;
                }
                else {
                    nodoAnterior.setSig(nodoActual.getSig());
                    nodoActual.setSig(null);
                    
                    nodoActual = nodoAnterior.getSig();
                }
                this.tamanio--;
            }
            else {
                nodoAnterior = nodoActual;
                nodoActual = nodoActual.getSig();
            }
        }
    }   

    
    
    
    public void eliminarMayorABC(int a, int b, int c)throws Exception{
        
        try{
            System.out.println("A["+a+"] B["+b+"] C["+c+"]");
    Nodo<T>nodoAnt=cabeza;
    Nodo<T>nodoSig=null;
    int mayor=0;
    
    if(nodoAnt.getSig()!=null){
     nodoSig=nodoAnt.getSig();
    }
    
    if(a>b&&a>c){mayor=a;}else{
      if(b>a&&b>c){mayor=b;}else{
        if(c>a&&c>b){mayor=c;}else{
          System.out.println("Mayor"+mayor);
      }
     }
    }
    
         if(nodoAnt.getInfo().equals(mayor)){
           nodoAnt.setSig(null);
            cabeza=nodoAnt.getSig();
             tamanio--;
           return;
    }else{
           while(nodoSig!=null){
            if(nodoSig.getInfo().equals(mayor)){
             nodoAnt.setSig(nodoSig.getSig());
              System.out.println("Encontro ["+nodoSig.getInfo()+"]");
               nodoSig.setSig(null);
                tamanio--;
             break;
            
    }else{
        nodoAnt=nodoSig;    
        nodoSig=nodoAnt.getSig();
        
    }
   }
  }
   
 }catch(Exception ex){
            System.out.println("LA LISTA ES VACIA");
}
    }

 public boolean moverMayor_al_Fin()
    {
     /**
      * Casos:
            1. L=<>   --> L.moverMayor()->No proceso-Vacía
            2. L=<2,4,56,7> --> L.moverMayor() --> L=<2,4,7,56> return true
            3. L=<2,4,56,7,56> --> L.moverMayor() --> L=<2,4,56,7,56> return false
            4. L=<2> --> L.moverMayor() --> L=<2>  return false
      */   
    //Validación caso 1 y 4
       if(this.esVacia() || this.getTamanio()==1) 
           throw new RuntimeException("No es posible realizar proceso de cambio de mayor");
        
        
       
       Nodo<T> nodoMayor,nodoUltimo,nodoAnt_May; 
       nodoUltimo=nodoMayor=this.cabeza;
       nodoAnt_May=null;
       boolean unMayor=true;
       for(Nodo<T> nodoActual=nodoMayor.getSig();nodoActual!=null;nodoActual=nodoActual.getSig())
       {
           //esto es un error :(
            /**
             * IF NODOACTUAL > NODOMAYOR)
             * NODMAYOR=NODOACTUAL
             */
           int comparador=((Comparable)nodoActual.getInfo()).compareTo(nodoMayor.getInfo());
           if(comparador>0)
           {
               nodoAnt_May=nodoUltimo;
               nodoMayor=nodoActual;
              //L=<2,30,40,30,50,40,1,2,1,2,50,100,2>
               unMayor=true;
                   
           }
           else
           {
                if(comparador==0)
                {
                   unMayor=false; 
                }
           }
           
           
         nodoUltimo=nodoActual;  
       }
       //proceso de desUNir e Unir
       
       if(!unMayor)
           throw new RuntimeException("No es posible realizar proceso de cambio de mayor por que está repetido");
       
       //Esté es el caso del mayor está en el nodoCabeza
       
     if(nodoMayor.getSig()!=null)  
     {    
            if(nodoAnt_May==null)
            {
                this.cabeza=nodoMayor.getSig();

            }
            else
            {
                 nodoAnt_May.setSig(nodoMayor.getSig());

            }
             nodoUltimo.setSig(nodoMayor);
             nodoMayor.setSig(null);
      }

        System.out.println("El mayor es:"+nodoMayor.getInfo().toString());
        return unMayor;
    }
    
    public boolean contieneElementosRepetidos()throws Exception{
        if(this.tamanio<2){
        throw new Exception("La lista no puede tener repetidos porque tiene menos de dos datos");
        }
        Nodo<T> nodoAnt=this.cabeza;
        Nodo<T> nodoActual=nodoAnt.getSig();
        
        while(nodoAnt!=null){
        
        while(nodoActual!=null){
          if(nodoAnt.getInfo().equals(nodoActual.getInfo())){
            return true;
          }
             nodoActual=nodoActual.getSig();
        }
          nodoAnt=nodoAnt.getSig();
          if(nodoAnt!=null){
          nodoActual=nodoAnt.getSig();
          }
        }
        return false;
        }
    
    
    public void unirL1ConL2(ListaS<T> l2){
    
    }
 
 
 
 
    
    
    
    
    
    /*
public void ordenarListaMenorAMayor(int a, int b, int c){
	Nodo<T>nodoAnt=cabeza;
	Nodo<T>nodoSig=cabeza.getSig();
	int mayor=0;
	
	if(a>b && a>c)
		mayor=a;
	
	else if(b>a && b>c)
		mayor= b;
	else
		mayor=c;                   //   5-->4-->3

	if(this.cabeza.getInfo().equals(mayor)){
	          cabeza=nodoSig;
	          tamanio--;
                          return;
	}

        while(nodoSig!=null){
        if(nodoSig.getInfo().equals(mayor)){
            nodoAnt.setSig(nodoSig.getSig());
            System.out.println("yei ["+nodoSig.getInfo()+"]");
            tamanio--;
            return;
            }
	nodoAnt=nodoAnt.getSig();
	nodoSig=nodoSig.getSig();
	}
    }*/  
}
    
//https://gitlab.com/danielomartb/estructuras_dinamicas_secuenciales.git