/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Invitado
 */
public class PruebaPila_Cola {

    public static void main(String[] args) {
        Pila<Integer> p = new Pila();
        Cola<Integer> c = new Cola();
        for (int i = 1; i < 15; i++) {
            p.apilar(i);
            c.enColar(i);
        }

        System.out.println("Pila:");
        //Imprimir:
        while (!p.esVacio()) {
            System.out.print(p.desapilar() + "\t");
        }

        System.out.println("\nCola:");
        while (!c.esVacio()) {
            System.out.print(c.deColar() + "\t");
        }
    }

}
