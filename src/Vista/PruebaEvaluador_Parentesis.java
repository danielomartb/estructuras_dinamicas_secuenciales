/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.EvaluadorExpresion;
import java.util.Scanner;

/**
 *
 * @author Invitado
 */
public class PruebaEvaluador_Parentesis {
    
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        System.out.println("Digite una expresión matemática:");
        String expresion=in.nextLine();
        
        EvaluadorExpresion evaluador=new EvaluadorExpresion(expresion);
        if(evaluador.evaluarParentesis())
            System.out.println(":) SU expresión tiene paréntesis ");
        else
            System.err.println(":( SU expresión NO tiene paréntesis ");
        
    }
}
