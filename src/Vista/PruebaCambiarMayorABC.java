/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import ufps.util.colecciones_seed.ListaS;
/**
 *
 * @author Famy
 */
public class PruebaCambiarMayorABC {
    public static void main(String[] args) throws Exception {
       
        //******************      /    LISTA 1    /       *************************
        
        ListaS<Integer> abc=new ListaS();
        abc.insertarInicio(2);
        abc.insertarInicio(1);
        abc.insertarInicio(4);
        abc.insertarInicio(5);
        abc.insertarInicio(3);
        abc.insertarInicio(7);
        abc.insertarInicio(25);
        abc.insertarInicio(12);
        abc.insertarInicio(11);
        
        
        System.out.println("Mi lista es: "+abc.toString());
        System.out.println("Tamaño de la Lista:"+abc.getTamanio());
        
        abc.eliminarMayorABC(2, 1, 0);
        System.out.println("Mi lista es: "+abc.toString());
        System.out.println("Tamaño de la Lista:"+abc.getTamanio()+"\n");
        
         //******************      /    LISTA 2    /       *************************
         
         
         
       ListaS<Integer> abc2=new ListaS();
        abc2.insertarInicio(2);
        abc2.insertarInicio(1);
        abc2.insertarInicio(4);
        abc2.insertarInicio(5);
        abc2.insertarInicio(3);
        abc2.insertarInicio(7);
        abc2.insertarInicio(25);
        abc2.insertarInicio(12);
        abc2.insertarInicio(11);
        
        System.out.println("Mi lista es: "+abc2.toString());
        System.out.println("Tamaño de la Lista:"+abc2.getTamanio());

        abc2.eliminarMayorABC(12, 11, 7);
        System.out.println("Mi lista es: "+abc2.toString());
        System.out.println("Tamaño de la Lista:"+abc2.getTamanio()+"\n");
        
         //******************      /    LISTA 3    /       *************************
        
         ListaS<Integer> abc3=new ListaS();
        abc3.insertarInicio(2);
        abc3.insertarInicio(1);
        abc3.insertarInicio(4);
        abc3.insertarInicio(5);
        abc3.insertarInicio(3);
        abc3.insertarInicio(7);
        abc3.insertarInicio(25);
        abc3.insertarInicio(12);
        abc3.insertarInicio(11);
        
        System.out.println("Mi lista es: "+abc3.toString());
        System.out.println("Tamaño de la Lista:"+abc3.getTamanio());

        abc3.eliminarMayorABC(11, 12, 25);
        System.out.println("Mi lista es: "+abc3.toString());
        System.out.println("Tamaño de la Lista:"+abc3.getTamanio()+"\n");
        
         //******************      /    LISTA 4    /       *************************
        
        ListaS<Integer> abc4=new ListaS();
        abc4.insertarInicio(5);
        abc4.insertarInicio(4);
        abc4.insertarInicio(3);
        
        System.out.println("Mi lista es: "+abc4.toString());
        System.out.println("Tamaño de la Lista:"+abc4.getTamanio());
        
        abc4.eliminarMayorABC(5, 4, 3);
        System.out.println("Mi lista es: "+abc4.toString());
        System.out.println("Tamaño de la Lista:"+abc4.getTamanio()+"\n");
        
         //******************      /    LISTA 5    /       *************************
        
        ListaS<Integer> abc5=new ListaS();
        
        abc5.eliminarMayorABC(5, 4, 3);
        System.out.println("Mi lista es: "+abc5.toString());
        System.out.println("Tamaño de la Lista:"+abc5.getTamanio());
    }
 
}
