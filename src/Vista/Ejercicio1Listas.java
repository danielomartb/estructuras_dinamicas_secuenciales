/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author Famy
 */
public class Ejercicio1Listas {
    
    public static void main(String[] args) throws Exception {
         ListaS<Integer> l1=new ListaS();
         ListaS<Integer> l2=new ListaS();
         int numero;
         int tamañoListas;
         Scanner sc = new Scanner(System.in);
          
        System.out.println("Digite el tamaño deseado de Lista 1 y Lista 2 ");
        tamañoListas=sc.nextInt();
        
        for(int i=0;i<tamañoListas;i++){
            numero = (int)(Math. random()*10+1);
            l1.insertarFin(numero);
            numero = (int)(Math. random()*10+1);
            l2.insertarFin(numero);   
          }
        /*private boolean contieneElementosRepetidos(){
        
        return false;
        }*/
        
        System.out.println("Mi Lista 1 es: "+l1.toString()+"\n");
        System.out.println("Mi Lista 2 es: "+l2.toString());
        System.out.println("La Lista 1 contiene elementos repetidos: "+l1.contieneElementosRepetidos());
        System.out.println("La Lista 2 contiene elementos repetidos: "+l2.contieneElementosRepetidos());
        
    }

}
