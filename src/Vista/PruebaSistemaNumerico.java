/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SistemaNumerico;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author Famy
 */
public class PruebaSistemaNumerico {
     public static void main(String[]args){
       pruebaUnion();         
    }
    
    public static void pruebaUnion(){

        System.out.println("Los datos de las listas se generaron aleatoreamente desde 1 hasta: "+9);
        SistemaNumerico sn= new SistemaNumerico(15, 12);
        
        System.out.println("Lista 1: "+sn.getMyLista1().toString());
        System.out.println("Lista 2_ "+sn.getMyLista2().toString());
        System.out.println("Union: "+sn.getUnion().toString());
        System.out.println("Interseccion: "+sn.getInterseccion().toString());
        System.out.println("Diferencia:"+sn.getDiferencia((ListaCD<Integer>) sn.getMyLista1()).toString());
        System.out.println("Diferencia Asimetrica: "+sn.getDiferenciaAsimetrica().toString());
    }
}
