/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.Cola;

/**
 *
 * @author Famy
 */
public class Ejercicio1G {

    public static void main(String[] args) {
        Cola<Character> c = leerCadena("Pila de la cual quieres obtener el dato del fondo");

        System.out.println("Pila en la que se buscará el ultimo dato: " + c.toString() + "\n");
        System.out.println("El ultimo dato de la pila es: " + ultimoDatoPila(c.clonarCola()) + "\n");
        System.out.println("Pila despues de haber buscado el dato: " + c.toString() + "\n");
    }

    private static Cola<Character> leerCadena(String mensaje) {
        Cola<Character> c = new Cola();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            c.enColar(cadena.charAt(i));
        }
        return c;
    }

    private static Character ultimoDatoPila(Cola<Character> c1) {

        Character ultimoDato = 0;
        ultimoDato = c1.deColar().charValue();

        return ultimoDato;
    }
}
