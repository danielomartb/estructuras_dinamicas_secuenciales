/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;
import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Famy
 */
public class Ejercicio1A {

    public static void main(String[] args) {
        Cola<Character> c1 = leerCadenaCola("Digite la cola 1");
        Cola<Character> c2 = leerCadenaCola("Digite a concatenar con cola 1");
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite el tamaño de la pila 1");
        Integer tamañoPila = sc.nextInt();

        Pila<Integer> p = crearPilaRandom(tamañoPila);
        System.out.println("Digite el tamaño de la pila 2");
        tamañoPila = sc.nextInt();
        Pila<Integer> p2 = crearPilaRandom(tamañoPila);

        System.out.println("Pila 1: " + p.toString());
        System.out.println("Pila 2: " + p2.toString());
        System.out.println("Pilas concatenadas: " + concatenarPilas(p, p2).toString() + "\n");
        System.out.println("Cola 1: " + c1.toString());
        System.out.println("Cola a concatenar: " + c2.toString());
        System.out.println("Colas Concatenadas: " + concatenarColas(c1, c2).toString());

    }

    public static Pila crearPilaRandom(Integer tamañoPila) {
        Pila<Integer> pilaRandom = new Pila<>();
        int dato = 0;
        for (int i = 0; i < tamañoPila; i++) {
            dato = (int) ((Math.random()) * 10) + 1;
            pilaRandom.apilar(dato);
        }
        return pilaRandom;
    }

    private static Pila<Integer> invertirPila(Pila<Integer> p1) {
        Pila<Integer> p2 = new Pila<>();
        while (!p1.esVacio()) {
            p2.apilar(p1.desapilar().intValue());
        }
        return p2;
    }

    public static Pila concatenarPilas(Pila<Integer> p1, Pila<Integer> p2) {

        int dato;
        p2 = invertirPila(p2);
        while (!p2.esVacio()) {
            dato = p2.desapilar().intValue();
            p1.apilar(dato);

        }
        p1.toString();
        return p1;
    }

    public static Cola concatenarColas(Cola<Character> c1, Cola<Character> c2) {

        while (!c2.esVacio()) {
            c1.enColar(c2.deColar().charValue());
        }
        return c1;
    }

    private static Cola<Character> leerCadenaCola(String mensaje) {
        Cola<Character> c = new Cola();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            c.enColar(cadena.charAt(i));
        }
        return c;
    }
}
