 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class PruebaListaSimple {
    
    
    public static void main(String[] args) throws Exception {
        ListaS<String> nombres=new ListaS();
        nombres.insertarInicio("Daniel");
        nombres.insertarInicio("Hernando");
        nombres.insertarInicio("Jorge");
        nombres.insertarInicio("Angel");
        
        System.out.println("Mi lista es: "+nombres.toString());
        System.out.println("Tamaño de la Lista:"+nombres.getTamanio());
        
        
        ListaS<Integer> numeros=new ListaS();
        numeros.insertarFin(1);
        numeros.insertarFin(10);
        numeros.insertarFin(100);
        numeros.insertarFin(1000);
        numeros.insertarFin(1);
        numeros.insertarFin(10);
        numeros.insertarFin(100);
        numeros.insertarFin(1000);
        numeros.insertarFin(1);
        numeros.insertarFin(10);
        numeros.insertarFin(100);
        numeros.insertarFin(1000);
        numeros.insertarFin(1);
        numeros.insertarFin(10);
        numeros.insertarFin(100);
        numeros.insertarFin(1000);
        System.out.println("Usando insertar al Fin:");
        System.out.println("Mi lista es: "+numeros.toString());
        System.out.println("Tamaño de la Lista:"+numeros.getTamanio());
        
        numeros.set(1, numeros.get(1)*900);
        System.out.println("Mi lista cambiando pos=1: "+numeros.toString());
        System.out.println("Generando un error con pos=20:"+numeros.get(20));
        
        System.out.println("Borrando el nodo de enteros con pos=2");
        System.out.println("Pos=2-->"+numeros.eliminar(2));
        System.out.println("Mi lista cambiada con un dato borrado: "+numeros.toString());
        System.out.println("Tamaño de la Lista:"+numeros.getTamanio());
        
        
        numeros.eliminarRepetido();
        System.out.println("Mi lista cambiada con borrado de repetidos: "+numeros.toString());
        System.out.println("Tamaño de la Lista:"+numeros.getTamanio());
    }
}
