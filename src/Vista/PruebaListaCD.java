/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class PruebaListaCD {

    public static void main(String[] args) {

        probarInsertarInicio();
        probarInsertarFin();
        probarBorrar();
    }

    private static void probarBorrar() {
        ListaCD<Integer> enteros = new ListaCD();
        for (int i = 1; i < 20; i += 2) {
            enteros.insertarFin(i);
        }

        System.out.println("********* Lista de ejemplo:");
        System.out.println(enteros.toString());
        System.out.println("Borrando elemento 0:" + enteros.eliminar(0));
        System.out.println("Borrando elemento 0:" + enteros.eliminar(enteros.getTamanio() - 1));
        System.out.println("Lista queda:" + enteros.toString());

    }

    private static void probarInsertarInicio() {
        ListaCD<Integer> enteros = new ListaCD();
        for (int i = 1; i < 20; i += 2) {
            enteros.insertarInicio(i);
        }
        System.out.println("Mi lista de enteros con insertarInicio:" + enteros.toString());
    }

    private static void probarInsertarFin() {
        ListaCD<Integer> enteros = new ListaCD();
        for (int i = 1; i < 20; i += 2) {
            enteros.insertarFin(i);
        }
        System.out.println("Mi lista de enteros con insertarFin:" + enteros.toString());
    }
}
