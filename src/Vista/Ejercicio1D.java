/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Famy
 */
public class Ejercicio1D {

    public static void main(String[] args) {

        Pila<Integer> p1 = crearPilaRandom();
        Integer X = leerEntero("Digite el dato del cual se buscaran los > y < de la pila");
        Pila<Integer> mayoresAX = mayoresAX(p1, X);
        Pila<Integer> menoresAX = menoresAX(p1, X);
        // mayoresAX=mayoresAX(p1, X);
        //menoresAX=menoresAX(p1, X);

        System.out.println("Pila Original: " + p1.toString() + "\n");
        System.out.println("Pila de mayores que " + X + ": " + mayoresAX.toString() + "\n");
        System.out.println("Pila de menores que " + X + ": " + menoresAX.toString() + "\n");

    }

    public static int leerEntero(String msj) {
        String dato = JOptionPane.showInputDialog(msj);
        int datoLeido = Integer.valueOf(dato);
        return datoLeido;
    }

    public static Pila crearPilaRandom() {
        int tamañoPila = leerEntero("Digite el tamaño de la´pila");
        System.out.println("tamaño:" + tamañoPila);
        Pila<Integer> pilaRandom = new Pila<>();
        int dato = 0;
        for (int i = 0; i < tamañoPila; i++) {
            dato = (int) ((Math.random()) * 20) + 1;
            pilaRandom.apilar(dato);
        }
        return pilaRandom;
    }

    public static Pila mayoresAX(Pila<Integer> p1, Integer x) {
        Pila<Integer> pilaRetorno = new Pila<>();
        Integer dato = 0;
        Pila<Integer> pCopia = p1.clonarPila();
        while (!pCopia.esVacio()) {
            dato = pCopia.desapilar().intValue();
            if (dato > x) {
                pilaRetorno.apilar(dato);
            }
        }
        return pilaRetorno;
    }

    public static Pila menoresAX(Pila<Integer> p1, Integer x) {
        Pila<Integer> pilaRetorno = new Pila<>();
        Integer dato = 0;
        Pila<Integer> pCopia = p1.clonarPila();
        while (!pCopia.esVacio()) {
            dato = pCopia.desapilar().intValue();
            if (dato < x) {
                pilaRetorno.apilar(dato);
            }
        }
        return pilaRetorno;
    }
}
