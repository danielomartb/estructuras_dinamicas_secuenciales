/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Famy
 */
public class Ejercicio1B {

    public static void main(String[] args) {
        Cola<Character> c = leerCadenaCola("Digite la cola");
        Pila<Character> p = leerCadena("Digite la pila");
        String datoABuscarPila = JOptionPane.showInputDialog("DATO A BUSCAR EN LA PILA Y COLA");
        System.out.println("Pila donde se buscara el dato: " + p.toString() + "\nDato a buscar: " + datoABuscarPila);
        System.out.println("Dato encontrado en la Pila: " + datoEncontradoP(p, datoABuscarPila) + "\nPila despues de buscar el dato: " + p.toString());
        System.out.println("\nCola donde se buscara el dato: " + c.toString() + "\nDato a buscar: " + datoABuscarPila);
        System.out.println("Dato encontrado en la Cola: " + datoEncontradoC(c, datoABuscarPila) + "\nCola despues de buscar el dato: " + c.toString());
    }

    private static Pila<Character> leerCadena(String mensaje) {
        Pila<Character> p = new Pila();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            p.apilar(cadena.charAt(i));
        }
        return p;
    }

    private static boolean datoEncontradoP(Pila<Character> p1, String datoABuscar) {
        boolean datoEncontrado = false;
        Character dato;
        Pila<Character> p2 = p1.clonarPila();
        while (!p2.esVacio()) {
            dato = p2.desapilar().charValue();
            if (datoABuscar.charAt(0) == dato) {
                datoEncontrado = true;
                break;
            }
        }
        return datoEncontrado;
    }

    private static boolean datoEncontradoC(Cola<Character> c1, String datoABuscar) {
        boolean datoEncontrado = false;
        Character dato;
        Cola<Character> c2 = c1.clonarCola();
        while (!c2.esVacio()) {
            dato = c2.deColar().charValue();
            if (datoABuscar.charAt(0) == dato) {
                datoEncontrado = true;
                break;
            }
        }
        return datoEncontrado;
    }

    private static Cola<Character> leerCadenaCola(String mensaje) {
        Cola<Character> c = new Cola();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            c.enColar(cadena.charAt(i));
        }
        return c;
    }
}
