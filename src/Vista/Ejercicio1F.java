/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Famy
 */
public class Ejercicio1F  {
    public static void main(String[] args) {
        Pila<Character> p = leerCadena("Pila de la cual quieres obtenr el dato del fondo");
        
        System.out.println("Pila en la que se buscará el ultimo dato: "+ p.toString()+"\n");
        System.out.println("El ultimo dato de la pila es: "+ ultimoDatoPila(p)+"\n");
        System.out.println("Pila despues de haber buscado el dato: "+ p.toString()+"\n");
    }
      private static Pila<Character> leerCadena(String mensaje) {
        Pila<Character> p = new Pila();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            p.apilar(cadena.charAt(i));
        }
        return p;
    }

    private static Character ultimoDatoPila(Pila<Character> p1) {

        Pila<Character> p3 = p1.clonarPila();
        Character ultimoDato = 0;
        while (!p3.esVacio()) {
            ultimoDato = p3.desapilar().charValue();
        }
        return ultimoDato;
    }
}
