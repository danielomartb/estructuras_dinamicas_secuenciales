/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;

/**
 * Clase que muestra la funcionalidad de los métodos genéricos
 * @author madarme
 */
public class PruebaGenericidad {
    
    public static void main(String[] args) {
        Integer vector[]={11,2,-3,4};
        String vector2[]={"anderson","angel","daniel","jorge","jefferson"};
        Punto vector3[]={new Punto(13,5), new Punto(6,7)};
        
        ordenar(vector);
        imprimir(vector);
        ordenar(vector2);
        imprimir(vector2);
        
        ordenar(vector3);
        imprimir (vector3);
    }
 
    //Creamos método genérico para imprimir sus datos
    // mi clase debe tener un requisito: toString
    
    public static <T> void imprimir (T vector[])
    {
        String msg="";
        for (T dato: vector)
            msg+=dato.toString()+",\t";
        
        System.out.println("Mi vector es:"+msg);
    }
    
    public static <T> void ordenar(T vector[])
    {
        T aux;
       for(int i = 0 ; i < (vector.length - 1) ; i++){
            for (int j = 0; j < (vector.length - 1); j++) {
                
                // T hereda de object
                
                T dato2 = vector[j+1];
                // CompareTo --> Comparable
                int comparador = ((Comparable)(vector[j])).compareTo(dato2); 
                if(comparador == 1){
                    aux = vector[j];
                    vector[j] = vector[j+1];
                    vector[j+1] = aux;
                }
            }
        }
    }

    
    
    
}
