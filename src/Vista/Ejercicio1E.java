/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Famy
 */
public class Ejercicio1E {

    public static void main(String[] args) {
        Pila<Character> p = leerCadena("Digite la pila a invertir");
        Cola<Character> c = leerCadenaCola("Digite la cola a invertir");

        System.out.println("Pila a invertir: " + p.toString());
        System.out.println("Pila Invertida: " + invertirPila(p) + "\n");
        System.out.println("Cola a invertir: " + c.toString());
        System.out.println("Cola Invertida: " + invertirCola(c));
    }

    private static Pila<Character> leerCadena(String mensaje) {
        Pila<Character> p = new Pila();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            p.apilar(cadena.charAt(i));
        }
        return p;
    }

    private static Cola<Character> leerCadenaCola(String mensaje) {
        Cola<Character> c = new Cola();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            c.enColar(cadena.charAt(i));
        }
        return c;
    }

    private static Pila<Character> invertirPila(Pila<Character> p1) {
        Pila<Character> p2 = new Pila<>();
        while (!p1.esVacio()) {
            p2.apilar(p1.desapilar().charValue());
        }
        return p2;
    }

    private static Cola<Character> invertirCola(Cola<Character> c) {
        Pila<Character> p = pasarColaAPila(c);
        while (!p.esVacio()) {
            c.enColar(p.desapilar().charValue());
        }
        return c;
    }

    private static Pila<Character> pasarColaAPila(Cola<Character> c) {
        Pila<Character> p = new Pila<>();
        while (!c.esVacio()) {
            p.apilar(c.deColar().charValue());
        }
        return p;
    }
}
