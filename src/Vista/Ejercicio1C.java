/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author Famy
 */
public class Ejercicio1C {

    public static void main(String[] args) {
        Pila<Character> p = leerCadena("Digite la Pila");
        Cola<Character> c = leerCadenaCola("Digite la Cola");
        String datoABuscar = JOptionPane.showInputDialog("DATO A ELIMINAR DE LA PILA Y COLA");
        
        System.out.println("Dato a buscar y eliminar n veces en la pila y en la cola: " + datoABuscar+"\n");
        System.out.println("Pila donde se realizará la busqueda y eliminacion: " + p.toString());
        System.out.println("Pila despues de eliminar el dato enviado n veces: " + eliminarDatoPila(p, datoABuscar).toString() + "\n");
        System.out.println("Cola donde se realizará la busqueda y eliminacion: " + c.toString());
        System.out.println("Cola despues de eliminar el dato enviado n veces: "+eliminarDatoCola(c, datoABuscar).toString());
    }

    private static Pila<Character> leerCadena(String mensaje) {
        Pila<Character> p = new Pila();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            p.apilar(cadena.charAt(i));
        }
        return p;
    }

    private static Pila<Character> invertirPila(Pila<Character> p1) {
        Pila<Character> p2 = new Pila<>();
        while (!p1.esVacio()) {
            p2.apilar(p1.desapilar().charValue());
        }
        return p2;
    }

    private static Pila eliminarDatoPila(Pila<Character> p1, String datoABuscar) {
        Pila<Character> p2 = new Pila<>();
        Character dato;
        while (!p1.esVacio()) {
            dato = p1.desapilar().charValue();
            if (datoABuscar.charAt(0) != dato) {
                p2.apilar(dato);
            }
        }
        p1 = invertirPila(p2);
        return p1;
    }

    private static Cola eliminarDatoCola(Cola<Character> c1, String datoABuscar) {
        Cola<Character> c2 = new Cola<>();
        Character dato;
        while (!c1.esVacio()) {
            dato = c1.deColar().charValue();
            if (datoABuscar.charAt(0) != dato) {
                c2.enColar(dato);
            }
        }
       
        return c2;
    }

    private static Cola<Character> leerCadenaCola(String mensaje) {
        Cola<Character> c = new Cola();
        String cadena = JOptionPane.showInputDialog(mensaje);
        for (int i = 0; i < cadena.length(); i++) {
            c.enColar(cadena.charAt(i));
        }
        return c;
    }

}
